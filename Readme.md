# Alternative2-List

Le dépôt contient toutes les alternatives proposées pour la plateforme Alternative2.



## Quels sont les principaux critères de sélection ?

- La solution est open source : son code source est auditable.

- La solution fait ce qu'elle dit qu'elle fait.

- La solution marche de manière stable pour assurer à l'utilisateur final qu'il n'aura pas à se battre avec des bugs inattendus.

- La solution proposée est unique : si deux logiciels existent pour faire la même chose, nous gardons celui qui apportera la plus grande facilité à l'utilisateur final.

## Quels sont les informations répertoriées pour chaque logiciel ?

- Son nom.

- Une description de ce qu'il fait.

- Une liste de catégorie auquel il appartient (d'avantage de précision à venir sur les catégories).

- Une liste de logiciel concurent que la solution permet de remplacer.

- La liste des plateformes sur laquelle la solution s'installe, ainsi que ça facilité d'installation sur chacun d'entres elles (0: en une opération → 5:compilation à la main).

- Le niveau de difficulté pour utiliser la solution (0: très facile → 5: nécessite un vrai apprentissage et de la pratique).

- Une liste de lien directement en rapport avec la solution (ex : son site officiel, son dépôt de code source).

- Le lien vers l'icône de ce logiciel.

- Une liste de capture d'écran présentant la solution.

- Une courte documentation pour installer rapidement le logiciel (écrite en markdown).

## Un logiciel n'est pas dans la liste ?

Si vous n'avez pas trouvé l'outil de vos rêves, mais que vous savez qu'il existe, soumettez son nom et les liens qui présentent ces outils afin que nous puissions l'ajouter à notre liste.

**Pour faire ceci, merci de suivre les étapes suivantes :**

1. Aller dans la section "[Ticket](https://gitlab.com/alternative-is/alternative2-list/-/issues)" de ce dépôt.

2. Cherchez si un ticket n'existe pas sur le logiciel que vous souhaitez rajouter.
   
   1. S'il existe, vérifiez qu'il n'est pas déjà en cours de traitement.
   
   2. Si aucun ticket n'existe sur ce logiciel, vous pouvez continuer.

3. Créez un nouveau ticket présentant le logiciel avec les informations suivantes :
   
   1. Nom du logiciel.
   
   2. Si vous utilisez régulièrement ce logiciel.
   
   3. Un court paragraphe qui explique vos motivations pour ajouter ce logiciel, ainsi que les avantages et inconvénient que vous lui trouvez.
   
   4. Lien vers les sites officiels ou vers les dépôts de code.

4. Complèter le plus possible les informations avec la liste donnée au [paragraphe précédent](#quels-sont-les-informations-répertoriées-pour-chaque-logiciel-).

## Des informations sont à corriger sur un logiciel ?

Si vous constatez que certaines des informations concernant un logiciel ne sont pas correctes, veuillez suivre les étapes suivantes :

1. Aller dans la section "[Ticket](https://gitlab.com/alternative-is/alternative2-list/-/issues)" de ce dépôt.
2. Cherchez si un ticket n'existe pas sur le logiciel que vous souhaitez rajouter.
   1. S'il existe, utilisez celui-ci.
   2. S'il n'existe pas, créez en un.
3. Si le sujet a déjà été abordé, merci de ne pas surcharger ni déterrer le sujet si vous n'êtes pas sûr que ce soit absolument nécessaire. Sinon, n'hésitez pas à vous exprimer librement ;)
